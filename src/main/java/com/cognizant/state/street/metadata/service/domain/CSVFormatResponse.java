package com.cognizant.state.street.metadata.service.domain;

import java.util.ArrayList;
import java.util.List;

public class CSVFormatResponse {
	List<CSVFormat> colList = new ArrayList<CSVFormat>();
	public CSVFormatResponse(){
		
	}
	public void add(CSVFormat row){
		this.colList.add(row);
	}
	
	public List<CSVFormat> getAll(){
		return colList;
	}
}
