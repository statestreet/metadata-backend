package com.cognizant.state.street.metadata.service.domain;

public class CSVFormat {
	private String bus_dataset_name;
	private String column_name;
	private String column_data_type;
	private String column_desc;
	private String column_npi_ind;
	private String column_pci_ind;
	
	public CSVFormat(String ds_name, String col_name, String col_type, 
					String col_desc, String col_npi, String col_pci){
		this.bus_dataset_name = ds_name;
		this.column_name = col_name;
		this.column_data_type = col_type;
		this.column_desc = col_desc;
		this.column_npi_ind = col_npi;
		this.column_pci_ind = col_pci;
	}
	
	public CSVFormat(){
		
	}
	
	public void load(String row){
		String[] temp = row.split(",");
		this.bus_dataset_name = temp[0];
		this.column_name = temp[1];
		this.column_data_type = temp[2];
		this.column_desc = temp[3];
		this.column_npi_ind = temp[4];
		this.column_pci_ind = temp[5];
	}
	
	public String getBus_dataset_name() {
		return bus_dataset_name;
	}
	public void setBus_dataset_name(String bus_dataset_name) {
		this.bus_dataset_name = bus_dataset_name;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getColumn_data_type() {
		return column_data_type;
	}
	public void setColumn_data_type(String column_data_type) {
		this.column_data_type = column_data_type;
	}
	public String getColumn_desc() {
		return column_desc;
	}
	public void setColumn_desc(String column_desc) {
		this.column_desc = column_desc;
	}
	public String getColumn_npi_ind() {
		return column_npi_ind;
	}
	public void setColumn_npi_ind(String column_npi_ind) {
		this.column_npi_ind = column_npi_ind;
	}
	public String getColumn_pci_ind() {
		return column_pci_ind;
	}
	public void setColumn_pci_ind(String column_pci_ind) {
		this.column_pci_ind = column_pci_ind;
	}
	
}
