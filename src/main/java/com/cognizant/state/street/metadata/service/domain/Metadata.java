package com.cognizant.state.street.metadata.service.domain;
import javax.persistence.*;

@Entity
@Table(name="metadata")
@NamedQuery(name="Metadata.findAll", query="SELECT t FROM Metadata t")
public class Metadata {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int dataset_id;

	@Lob
	@Column(name="bus_dataset_name")
	private String busDatasetName;

	@Lob
	@Column(name="target_type")
	private String targetType;

	@Lob
	@Column(name="dataset_path")
	private String datasetPath;
	
	@Lob
	@Column(name="dataset_status")
	private String dataset_status;
	
	public String getDataset_status() {
		return dataset_status;
	}

	public void setDataset_status(String dataset_status) {
		this.dataset_status = dataset_status;
	}

	public Metadata() {
	}
	
	public int getDataset_id() {
		return dataset_id;
	}

	public void setDataset_id(int dataset_id) {
		this.dataset_id = dataset_id;
	}

	public String getBusDatasetName() {
		return busDatasetName;
	}

	public void setBusDatasetName(String busDatasetName) {
		this.busDatasetName = busDatasetName;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getDatasetPath() {
		return datasetPath;
	}

	public void setDatasetPath(String datasetPath) {
		this.datasetPath = datasetPath;
	}

}
