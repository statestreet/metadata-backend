package com.cognizant.state.street.metadata.service.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the metadata_cols database table.
 * 
 */
@Entity
@Table(name="metadata_cols")
@NamedQuery(name="MetadataCol.findAll", query="SELECT m FROM MetadataCol m")
public class MetadataCol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="col_id")
	private int colId;

	@Column(name="bus_dataset_name")
	private String busDatasetName;

	@Column(name="column_data_type")
	private String columnDataType;

	@Lob
	@Column(name="column_desc")
	private String columnDesc;

	@Column(name="column_name")
	private String columnName;

	@Column(name="column_npi_ind")
	private String columnNpiInd;

	@Column(name="column_pci_ind")
	private String columnPciInd;

	public MetadataCol() {
	}

	public int getColId() {
		return this.colId;
	}

	public void setColId(int colId) {
		this.colId = colId;
	}

	public String getBusDatasetName() {
		return this.busDatasetName;
	}

	public void setBusDatasetName(String busDatasetName) {
		this.busDatasetName = busDatasetName;
	}

	public String getColumnDataType() {
		return this.columnDataType;
	}

	public void setColumnDataType(String columnDataType) {
		this.columnDataType = columnDataType;
	}

	public String getColumnDesc() {
		return this.columnDesc;
	}

	public void setColumnDesc(String columnDesc) {
		this.columnDesc = columnDesc;
	}

	public String getColumnName() {
		return this.columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnNpiInd() {
		return this.columnNpiInd;
	}

	public void setColumnNpiInd(String columnNpiInd) {
		this.columnNpiInd = columnNpiInd;
	}

	public String getColumnPciInd() {
		return this.columnPciInd;
	}

	public void setColumnPciInd(String columnPciInd) {
		this.columnPciInd = columnPciInd;
	}

}