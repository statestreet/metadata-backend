package com.cognizant.state.street.metadata.service.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.cognizant.state.street.metadata.service.domain.MetadataCol;

@CrossOrigin(origins="*")
public interface MetadataColRepository extends CrudRepository<MetadataCol, Integer> {
	List<MetadataCol> findByBusDatasetName(@Param("dataset") String dataset);
}
