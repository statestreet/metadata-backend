package com.cognizant.state.street.metadata.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.cognizant.state.street.metdata.controller.MetadataController;

@SpringBootApplication
@ComponentScan(basePackageClasses = MetadataController.class)
public class MetadataStatestreetApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetadataStatestreetApplication.class, args);
	}
	
	@Bean
	public WebMvcConfigurerAdapter corsConfigurer(){
		return new WebMvcConfigurerAdapter(){
			@Override
			public void addCorsMappings(CorsRegistry registry){
				registry.addMapping("/metadatas").allowedOrigins("*");
				registry.addMapping("/").allowedOrigins("*");
				
			}
		};
	}
}
