package com.cognizant.state.street.metdata.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.cognizant.state.street.metadata.service.domain.CSVFormat;
import com.cognizant.state.street.metadata.service.domain.CSVFormatResponse;

@RestController
@CrossOrigin(origins = "*")
public class MetadataController {
	//private final String tempPath = "C:/Users/ajsiv/Downloads";
	private final String tempPath = "/temp/metadata";
	private List<CSVFormat> respList;
	OutputStream output = null;
	int read = 0;
	Logger log = Logger.getLogger(MetadataController.class);
	

    @RequestMapping(value="/csv/upload", method = RequestMethod.POST)
    public @ResponseBody List<CSVFormat> handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
    	long epoch = System.currentTimeMillis();
        try {
        	byte[] bytes = file.getBytes();
        	output = new FileOutputStream(new File(tempPath + "/" + String.valueOf(epoch) + ".csv"));
        	log.info(tempPath + "/" + String.valueOf(epoch) + ".csv");
        	IOUtils.write(bytes, output);
		} catch (IOException e) {
			
			redirectAttributes.addFlashAttribute("message",
	                "File Upload Failed with " + e.getMessage() + "!");
		} finally{
			try {
				log.info("Closing the file");
				output.close();
			} catch (IOException e) {
				System.out.println("File already closed - " + e.getMessage());
			}
		}
        
        respList = loadList(tempPath + "/" + String.valueOf(epoch) + ".csv");
        log.info(respList.toString());
        return respList;
    }
    
    private List<CSVFormat> loadList(String path){
    	log.info("Parsing the file");
    	String line = null;
    	CSVFormatResponse formatted = new CSVFormatResponse();
    	BufferedReader br = null;
    	try {
    		br = new BufferedReader(new FileReader(path));
			while ((line = br.readLine()) != null){
				CSVFormat rec = new CSVFormat();
				rec.load(line);
				formatted.add(rec);
			}
			
		} catch (IOException e) {
			System.out.println("Unable to open the file " + path);
		} finally{
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("File already closed : " + e.getMessage());
			}
		}
		return formatted.getAll();
    	
    }
}
